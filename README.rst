Usage
=====


.. code-block::

    # make inkex available
    export PYTHONPATH=`inkscape -x`
    # run
    python3 export-pdf.py