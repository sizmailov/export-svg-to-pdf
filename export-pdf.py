import inkex
import inkex.svg
import pathlib
import subprocess
from lxml import etree

input_filename = "./3pages-in-row.svg"
n_pages = 3
output_dir = pathlib.Path("./output")

doc = etree.parse(input_filename, parser=inkex.elements.SVG_PARSER)
root = doc.getroot()  # type: inkex.svg.SvgDocumentElement


def shift_all_children(by):
    for el in root:
        el.transform.add_translate(by)


if not output_dir.exists():
    output_dir.mkdir()

for page in range(n_pages):
    svg_output = output_dir / f"page-{page:03d}.svg"
    pdf_output = output_dir / f"page-{page:03d}.pdf"
    with svg_output.open("wb") as f:
        f.write(root.tostring())
    subprocess.call(["inkscape",
                     "--export-file", pdf_output,
                     svg_output
                     ])
    shift_all_children(by=(-root.width, 0))

